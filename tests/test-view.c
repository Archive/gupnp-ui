/* 
 * Copyright (C) 2007 OpenedHand Ltd.
 *
 * Author: Jorn Baayen <jorn@openedhand.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include <gtk/gtkmain.h>
#include <gtk/gtkscrolledwindow.h>
#include <gtk/gtkwindow.h>
#include <stdlib.h>
#include <locale.h>
#include <libgupnp-ui/gupnp-ui-device-view.h>

int
main (int argc, char **argv)
{
        GtkWidget *window, *sw, *view;
        GUPnPUIDeviceStore *store;
        GUPnPControlPoint *cp;
        GUPnPContext *context;
        GError *error;

        /* Init */
        g_thread_init (NULL);

        setlocale (LC_ALL, "");

        gtk_init (&argc, &argv);

        /* Create UPnP-related objects */
        error = NULL;
        context = gupnp_context_new (NULL, NULL, 0, &error);
        if (error) {
                g_printerr ("Error creating the GUPnP context: %s\n",
			    error->message);
                g_error_free (error);

                return EXIT_FAILURE;
        }

        cp = gupnp_control_point_new (context, "upnp:rootdevice");

        /* Create widgets */

        /* Window */
        window = gtk_window_new (GTK_WINDOW_TOPLEVEL);

        g_signal_connect (window,
                          "delete-event",
                          G_CALLBACK (gtk_main_quit),
                          NULL);

        gtk_window_resize (GTK_WINDOW (window), 400, 300);

        gtk_window_set_title (GTK_WINDOW (window), "UPnP devices");

        /* Scrolled window */
        sw = gtk_scrolled_window_new (NULL, NULL);

        gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (sw),
                                        GTK_POLICY_AUTOMATIC,
                                        GTK_POLICY_ALWAYS);
        gtk_scrolled_window_set_shadow_type (GTK_SCROLLED_WINDOW (sw),
                                             GTK_SHADOW_IN);

        gtk_container_add (GTK_CONTAINER (window), sw);

        /* Store and View */
        store = gupnp_ui_device_store_new (cp);

        view = gupnp_ui_device_view_new (store);

        gtk_container_add (GTK_CONTAINER (sw), view);

        /* Run */
        gssdp_resource_browser_set_active (GSSDP_RESOURCE_BROWSER (cp), TRUE);

        gtk_widget_show_all (window);

        gtk_main ();

        /* Cleanup */
        g_object_unref (cp);
        g_object_unref (context);

        return EXIT_SUCCESS;
}
